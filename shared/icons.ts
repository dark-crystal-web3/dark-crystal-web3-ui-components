import h from '../services/hyperscript'

export default function (iconName: string) {
  return iconName.length
    ? h('img', {
        src: `/img/${iconName}.png`,
        width: 40,
        height: 40,
        alt: `${iconName} icon`
      })
    : undefined
}
