export default {
  appName: 'Dark Crystal',
  appSubtext: 'Social Recovery Dapp',
  secretOwner: 'Secret Owner',
  recoveryPartner: 'Recovery Partner',
  pageHeadings: {
    backup: {
      text: 'Backup a secret',
      subtext:
        'To backup a secret you need to choose recovery partners and get their public keys.',
      subsubtext:
        'This will split your given secret into a set of encrypted backup pieces linked to your chosen partners and stored on the distributed web.',
    },
    recover: {
      text: 'Recover a secret',
      subtext:
        'To recover a secret you will need the unencrypted shares from the recovery partners.',
      subsubtext:
        'To recover a secret you need to give the identifier you used during backup (usually your Ethereum address) to your recovery partners and ask them to retrieve their backup piece and send it to you.',
    },
    recoverSuccess: {
      text: 'Your secret was recovered!',
      subtext: 'You successfully recovered your secret.',
    },
    store: {
      text: 'Your secret is safe now.',
      subtext: 'Successfully published your shares on the distributed web.',
    },
    storing: {
      text: 'Publishing backup...',
    },
    storeFailed: {
      text: 'Failed to publish backup!',
    }
  },
  publicKeysInput: {
    label: 'Public keys of recovery partners',
    subtext: [
      'You need to choose a set of contacts who you trust to be your **recovery partners**.',
      'Each of them **must** have an Ethereum account.',
      'Each of them must **generate a public encryption key** and send it to you by visiting [This page].',
    ],
    placeholder: "Recovery partner's encryption public key",
    badLength: 'Public keys must be 32 bytes hex.',
    duplicate: 'Duplicate public key. You have already entered this key.'
  },
  identifierInput: {
    label: 'Identifier',
    placeholder: 'Identifier for the secret',
    subtext: [
      'A **unique identifier** for your secret that will be used by your **recovery partners** to find the share to recover.',
      'Usually it is best to leave it as your **Ethereum address** but in some cases you may want to use a different identifier. [Read more]',
    ],
  },
  secretInput: {
    label: 'Secret',
    placeholder: 'Enter a secret',
    subtext: [
      'A secret could be anything. Think about it as **sensitive data** such as your gpg private key, the password to your encrypted harddrive, or your Ethereum seed phrase.',
    ]
  },
  addButtonText: 'Add one more',
  removeButtonText: 'Remove one',
  createBackupSubmit: 'Create backup',
  backupCreatedTitle: 'Backup Created!',
  showQrCodeButton: 'Show QR code',
  resetButton: 'Reset',
  decryptedSharesInput: {
    label: 'Shares from recovery partners',
    placeholder: 'A decrypted share',
    invalid: 'Shares must be valid hex and at least %d characters long.',
  },
  addShareButtonTitle: 'Add another share',
  recoverSecretButton: 'Recover secret',
  secretRecoveredTitle: 'Secret recovered successfully',
  copyToClipboardButtonTitle: 'Copy to clipboard',
  recoverInstruction: {
    label:
      'To recover a secret you need the decrypted shares from your original recovery partners',
    subtext: [
      'You need to send your **unique identifier** to your original contacts (recovery partners).',
      'Each of them must send you back their decryted share',
    ],
  },
  sendIdentifier: {
    label: 'Send identifier',
    subtext: [
      'Make sure you remember your **original partners**.',
      'Always use a **secure encrypted channel** for sending keys.',
    ],
  },
  addTestData: 'Add test data',
  thresholdText: [
    '%d of %d backup pieces will be needed to recover.',
    'Meaning %d out of %d friend\'s will need to decrypt their shares and send them to you.',
  ],
  publishAction: {
    label: 'Publish the encrypted pieces & store my secret on the distributed web.',
    subtext: [
      'The encrypted pieces of your secret will be published to the blockchain without revealing the identities of your recovery partners'
    ]
  },
  publishAndStoreButton: 'Publish and store',
  successfullyPublishedAction: {
    label: 'To recover the secret, ask your partners for their decrypted pieces.',
    subtext: [
      'Don\'t forget **who your partners** are.',
      'Always use a **secure channel** for sending keys.',
      'They will need the lookup key to decrypt their piece.'
    ],
  },
  recoveryDecryptionError: 'Cannot recover secret. Maybe you need more shares, or they were copied incorrectly.',
}
