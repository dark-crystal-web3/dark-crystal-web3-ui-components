import parse from 'html-react-parser'

// Adds bold using **markdown-style notation**
// Parses links with a given target and [link text given like this]

export default function (inputText: string, target?: string) {
   return parse(parseLink(addBold(inputText), target))
}

function addBold (inputText: string) {
  const segments = inputText.split('**')
  if (segments.length < 3) return inputText
  const bolded = segments.map((segment, index) => {
    return (index % 2 === 0)
      ? segment
      : `<strong>${segment}</strong>`
  })
  return bolded.join('')
}

function parseLink(inputText: string, target?: string) {
  if (!target) return inputText
  const linkStart = inputText.indexOf('[')
  if (linkStart < 0) return inputText
  const linkEnd = inputText.indexOf(']')
  if (linkEnd < 0) return inputText

  const beforeLink = inputText.slice(0, linkStart)
  const linkText = inputText.slice(linkStart + 1, linkEnd)
  const afterLink = inputText.slice(linkEnd + 1)

  return `${beforeLink}<a href="${target}" target="_blank">→ <u>${linkText}</u></a>${afterLink}`
}
