import React from 'react'

type content = string | React.ReactNode | Array<React.ReactNode>

export default function(element: any, p?: Object | content, c?: content) {
  let properties: Object | content
  let children: content

  if (c) {
    properties = p
    children = c
  } else {
    if (typeof p === 'string' || Array.isArray(p) || React.isValidElement(p)) {
      properties = {}
      children = p
    } else {
      if (typeof p === 'object') {
        properties = p
        children = null
      }
    }
  }

  const args: any = [element, properties].concat(children)
  return React.createElement.apply(React, args)
}
