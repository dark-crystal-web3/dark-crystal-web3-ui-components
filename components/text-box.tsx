import React from 'react'
import h from '../services/hyperscript'
// import Button from './button.tsx'
import icons from '../shared/icons'
import Text from '../services/text'

// Text box - looks like an input box but cannot be edited
// Has a copy to clipboard button.
// Content is passed either as dom elements (content), or a string (text)
// The string must always be given as it is used for copying.

type Props = {
  icon?: React.ReactNode,
  content?: React.ReactNode,
  text: string,
  hideCopyButton?: boolean
}

const TextBox = ({ icon, content, text, hideCopyButton }: Props) => {
  return h('div', { className: 'py-1' }, [
    h('div', { className: 'flex border-2 border-very-dark-pink rounded' }, [
      icon
        ? h('div',
          { className: 'bg-very-dark-pink md:flex' },
          h('span', { className: 'm-1' }, icon),
        )
        : undefined,
      h('div',
        { className: 'w-full mx-2 p-1.5 text-base my-1 font-mono break-all' },
        [ content || text ]
      ),
      hideCopyButton
        ? undefined
        : h( 'button', {
            className: `bg-very-dark-pink hover:bg-pink-600`,
            onClick: () => {
              navigator.clipboard.writeText(text)
            },
            title: Text.copyToClipboardButtonTitle,
          },
          [
            h('div', { className: 'md:flex' }, [
              h('span',
                { className: 'm-1' },
                icons('copy-papers'),
              )
            ]),
          ]
        )
    ])
  ])
}

export default TextBox
