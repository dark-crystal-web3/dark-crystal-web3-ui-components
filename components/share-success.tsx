import React from 'react'
import h from '../services/hyperscript'
import QRCode from 'qrcode'
import Threshold from './threshold'
import Button from './button'
import text from '../services/text'
import TextBox from './text-box'

// Share 'success' screen containing a link - only for use by the offline first version

type Props = {
  n: number
  m: number
  shareLink: string,
  reset: Function,
}

type State = {
  showingQrCode: boolean,
}

export default class ShareSuccess extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      showingQrCode: false,
    }
  }

  render() {
    return h('div', [
      h('h3',
        {
          className: 'font-silkscreen uppercase text-xl font-bold mb-4',
        },
        text.backupCreatedTitle
      ),
      h(Threshold, {
        n: this.props.n,
        m: this.props.m,
      }),
      h(TextBox, {
        content: h('a',
          { href: this.props.shareLink },
          h('span', { className: 'break-all hover:underline' }, this.props.shareLink)
        ),
        text: this.props.shareLink,
      }),
      h('canvas', { className: 'mt-4', id: 'qr-canvas', width: 0, height: 0 }),
      h('div', { className: 'flex justify-between mt-4' }, [
        h('div'),
        h('div', [
          this.state.showingQrCode
            ? undefined
            : h('span', { className: 'mr-4' }, [
                h(Button, {
                  onClick: () => {
                    this.setState({ showingQrCode: true })
                    this.showQrCode()
                  },
                  text: text.showQrCodeButton,
                })
              ]),
          h(Button, { onClick: this.props.reset, text: text.resetButton }),
        ]),
      ]),
    ])
  }

              // h('span', { className: 'mr-4' }, [
  showQrCode() {
    if (this.props.shareLink.length) {
      const canvas = document.getElementById('qr-canvas')
      QRCode.toCanvas(canvas, this.props.shareLink, function (error: any) {
        if (error) console.error(error)
      })
    }
  }
}
