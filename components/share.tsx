import React from 'react'
import h from '../services/hyperscript'
import Threshold from './threshold'
import text from '../services/text'
import TextInputHeader from './text-input-header'
import TextInput from './text-input'
import Button from './button'
import icons from '../shared/icons'

// Form for creating backups
// The default lookup key can be passed as the `ethAddress` property
// The `ShareSuccess` component is also passed in a a property, as this is different
// for the online and offline versions.

const MAX_SHARES = 255

type Props = {
  visible: boolean,
  ShareSuccess: React.ReactNode,
  ethAddress?: string,
  testMode: boolean,
  dc: any,  //TODO
}

type State = {
  publicKeys: Array<PublicKeyInput>,
  secret: string,
  lookupKey: string,
  lookupKeyModified: boolean,
  shareLink: string,
  shareErrMessage: string,
  showingQrCode: boolean,
  threshold: number,
  backup: {
    m: number,
    n: number,
  }
}


export default class Share extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      publicKeys: [new PublicKeyInput(), new PublicKeyInput()],
      secret: '',
      lookupKey: props.ethAddress || '',
      lookupKeyModified: false,
      shareLink: '',
      shareErrMessage: '',
      showingQrCode: false,
      threshold: 0,
      backup: { m: 0, n: 0 },
    }
    this.updateSecret = this.updateSecret.bind(this)
    this.updateLookupKey = this.updateLookupKey.bind(this)
    this.createBackup = this.createBackup.bind(this)
    this.addTestData = this.addTestData.bind(this)
  }

  static getDerivedStateFromProps(props: Props, state: State) {
    return (props.ethAddress && state.lookupKeyModified === false)
      ? { lookupKey: props.ethAddress }
      : null
  }

  render() {
    if (!this.props.visible) return

    return this.state.shareLink.length
      ? h(this.props.ShareSuccess, {
          shareLink: this.state.shareLink,
          m: this.state.backup.m,
          n: this.state.backup.n,
          reset: () => {
            this.setState({
              shareLink: '',
            })
          },
        })
      : h('div', [
          h(TextInputHeader, { text: text.publicKeysInput, number: 1, linkTarget: 'https://web3.darkcrystal.pw/generate' }),
          h('div', { className: 'mb-2' }, [
            this.state.publicKeys.map((publicKey, index) => {
              return h('div', { key: index }, [
                h(TextInput, {
                  placeholder: text.publicKeysInput.placeholder,
                  value: publicKey.inputString,
                  icon: icons('key44px'),
                  onChange: (event: React.ChangeEvent<HTMLInputElement>) => {
                    const existingPublicKeys = this.state.publicKeys
                    publicKey.updateInputString(event.target.value)
                    if (publicKey.isValid()) {
                      if (
                        existingPublicKeys.find(
                          (pk, i) =>
                            pk.inputString === publicKey.inputString &&
                            i !== index
                        )
                      ) {
                        publicKey.validity = 'duplicate'
                      }
                    }
                    this.setState({
                      publicKeys: existingPublicKeys,
                    })
                  },
                  errorMessage:
                    publicKey.inputString.length && !publicKey.isValid()
                      ? text.publicKeysInput[publicKey.validity]
                      : undefined,
                }),
              ])
            }),
            h('div', { className: 'my-1' }, [
              h('span', { className: 'mr-2'},
                h(Button, {
                  text: text.addButtonText,
                  textOutsideBox: true,
                  onClick: () => {
                    const existingPublicKeys = this.state.publicKeys
                    if (existingPublicKeys.length < MAX_SHARES) {
                      existingPublicKeys.push(new PublicKeyInput())
                    }
                    this.setState({ publicKeys: existingPublicKeys })
                  },
                  icon: icons('plus'),
                })
              ),
              h(Button, {
                text: text.removeButtonText,
                textOutsideBox: true,
                disabled: this.state.publicKeys.length < 3,
                onClick: () => {
                  const existingPublicKeys = this.state.publicKeys
                  existingPublicKeys.pop()
                  this.setState({ publicKeys: existingPublicKeys })
                },
                icon: icons('minus'),
              }),
            ])
          ]),
          h(Threshold, {
            n: this.state.publicKeys.filter((pk) => pk.isValid()).length,
            onChange: (threshold: number) => {
              this.setState({ threshold })
            },
          }),
          h('div', [
            h(TextInputHeader, { text: text.identifierInput, number: 2, linkTarget: 'https://darkcrystal.pw/web3' }),
            h(TextInput, {
              placeholder: text.identifierInput.placeholder,
              value: this.state.lookupKey,
              onChange: this.updateLookupKey,
              icon: icons('eth-logo'),
            }),
          ]),
          h('div', [
            h(TextInputHeader, { text: text.secretInput, number: 3 }),
            h(TextInput, {
              placeholder: text.secretInput.placeholder,
              value: this.state.secret,
              onChange: this.updateSecret,
              icon: icons('crystals-plenty'),
            }),
          ]),
          h('div', { className: 'flex justify-between mt-4' }, [
            h('div'),
            h('div', [
              h('span', { className: 'mr-4' }, [
                h(Button, {
                  onClick: this.createBackup,
                  disabled: !this.canBackup(),
                  text: text.createBackupSubmit,
                  icon: icons('crystals-3'),

                }),
              ]),
              this.props.testMode
                ? h(Button, {
                  className: 'mr-0',
                  onClick: this.addTestData,
                  text: text.addTestData,
                  icon: icons('plus'),
                })
                : undefined,
            ])
          ]),
          h('p.text-black', this.state.shareErrMessage),
        ])
  }

  updateSecret(event: React.ChangeEvent<HTMLInputElement>) {
    this.setState({ secret: event.target.value })
  }

  updateLookupKey(event: React.ChangeEvent<HTMLInputElement>) {
    this.setState({ lookupKey: event.target.value, lookupKeyModified: true })
  }

  // Are we able to backup yet?
  canBackup() {
    return (
      this.state.secret.length &&
      this.state.lookupKey.length &&
      this.state.publicKeys.filter((pk) => pk.isValid()).length > 1
    )
  }

  createBackup() {
    const resetInputFields = {
      secret: '',
      lookupKey: this.props.ethAddress || '',
      publicKeys: [new PublicKeyInput(), new PublicKeyInput()],
    }
    try {
      const shareLink = this.props.dc.share(
        this.state.publicKeys
          .filter((pk) => pk.isValid())
          .map((pk) => pk.inputString),
        this.state.secret,
        this.state.lookupKey,
        this.state.threshold
      )
      this.setState(
        Object.assign(resetInputFields, {
          shareLink,
          backup: {
            m: this.state.threshold,
            n: this.state.publicKeys.filter((pk) => pk.isValid()).length,
          },
        })
      )
    } catch (error) {
      const err = error as any // Should this be `Error`?
      console.log('Error from share fn', err)
      this.setState(
        Object.assign(resetInputFields, { shareErrMessage: err.message })
      )
    }
  }

  addTestData() {
    this.setState({
      secret:
        'spare human danger common patch pioneer security pond push purity wear crane',
      publicKeys: [
        new PublicKeyInput(
          'cf61ca2d786bc57cff2455003862ee70ca64d0dddb8b6ae0d1927e81411bdd21'
        ),
        new PublicKeyInput(
          'd92d16240856d9fa136712942943e0869e7d7b93e8b3417ee4cf20946e042b0e'
        ),
        new PublicKeyInput(
          'f326c7c66fd5e2af1dc535d3aac20cf2aec2ac22cd92de0e57a66c80a7a9ef50'
        ),
      ],
      lookupKey: (this.state.lookupKey && this.state.lookupKey.length)
        ? this.state.lookupKey
        : 'Some lookup key',
    })
  }
}


class PublicKeyInput {
  inputString: string
  validity: string

  constructor(inputString?: string) {
    this.inputString = ''  // Just to make typescript not bark
    this.validity = 'empty'
    this.updateInputString(inputString || '')
  }

  updateInputString(newInputString: string) {
    this.inputString = newInputString.trim()
    this.validity = Buffer.from(this.inputString, 'hex').length === 32
      ? 'valid'
      : 'badLength'
  }

  isValid() {
    return this.validity === 'valid'
  }
}
