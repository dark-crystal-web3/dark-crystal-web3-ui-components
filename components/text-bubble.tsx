import React from 'react'
import h from '../services/hyperscript'
import formatString from '../services/format-string'

// A bubble thing with some formatted bullet points in it

type Props = {
  additionalContent?: React.ReactNode,
  bullets: Array<string>,
  linkTarget?: string,
}

const TextBubble = ({ bullets, linkTarget, additionalContent }: Props) => {
  return h(
    'div',
    { className: 'rounded text-base text-very-dark-pink bg-pale-pink pl-6 pr-4 py-8 md:pl-20 md:pr-8 mb-5 mt-2' },
    [
      additionalContent,
      h(
        'ul',
        { className: 'list-disc list-inside' },
        bullets.map((t, i) => h('li', { key: i }, [formatString(t, linkTarget)]))
      ),
    ]
  )
}

export default TextBubble
