import React from 'react'
import h from '../services/hyperscript'
import NumberedHeading from './numbered-heading'
import TextBubble from './text-bubble'

// A form section containing a text input box, numbered heading, and optional
// 'subtext' section.

type Props = {
  number: number,
  text: {
    label: string,
    subtext?: Array<string>,
  },
  linkTarget?: string,
}
const TextInputHeader = ({ number, text, linkTarget }: Props) => {
  return h('div', [
    h(NumberedHeading, {
      number,
      text: text.label,
    }),
    text.subtext
      ? h(TextBubble, { bullets: text.subtext, linkTarget })
      : undefined,
  ])
}

export default TextInputHeader
