import React from 'react'
import h from '../services/hyperscript'
import Button from './button'
import text from '../services/text'
import TextInputHeader from './text-input-header'
import TextInput from './text-input'
import TextBox from './text-box'
import ErrorMessage from './error-message'
import icons from '../shared/icons'

const MIN_SHARE_LENGTH = 33 + 16
const MAX_SHARES = 255

/// For recovering backups

type Props = {
  visible: boolean,
  ethAddress?: string,
  testMode: boolean,
  dc: any, // TODO
  recoverContainer?: React.ReactNode,
}

type State = {
  shares: Array<ShareInput>,
  secret: string,
  recoveryErrMessage: string,
}

export default class Recover extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      shares: [new ShareInput(), new ShareInput()],
      secret: '',
      recoveryErrMessage: '',
    }

    this.addTestData = this.addTestData.bind(this)
    this.recover = this.recover.bind(this)
  }

  render() {
    if (!this.props.visible) return
    return h('div', [
      this.state.secret.length
        ? h(this.props.recoverContainer || 'div', { success: 'true' }, [
            this.props.recoverContainer
              ? undefined
              : h('h3', {
                  className:
                    'font-silkscreen text-very-dark-pink uppercase text-xl font-bold mb-4',
                  },
                  text.secretRecoveredTitle
                ),

            h(TextBox, {
              content: h('span', { className: 'break-all' }, this.state.secret), icon: icons('master-key'),
              text: this.state.secret,
            }),

            h('div', { className: 'flex justify-between mt-4' }, [
              h('div'),
              h('div', [
                h(Button, {
                  onClick: () => {
                    this.setState({ secret: '' })
                  },
                  text: text.resetButton,
                }),
              ]),
            ]),
          ])
        : h(this.props.recoverContainer || 'div', [
            h('div', [
              h(TextInputHeader, { text: text.recoverInstruction, number: 1 }),
              h(TextInputHeader, { text: text.sendIdentifier, number: 2 }),
              this.props.ethAddress
                ? h('div', [
                    h(TextBox, { text: this.props.ethAddress, icon: icons('eth-logo') }),
                  ])
                : undefined,
              h(TextInputHeader, {
                text: text.decryptedSharesInput,
                number: 3,
              }),
              this.state.shares.map((share, index) => {
                return h('div', { key: index }, [
                  h(TextInput, {
                    placeholder: text.decryptedSharesInput.placeholder,
                    value: share.inputString,
                    icon: icons('master-key'),
                    onChange: (event: React.ChangeEvent<HTMLInputElement>) => {
                      const existingShares = this.state.shares
                      share.updateInputString(event.target.value)
                      if (share.isValid) {
                        if (
                          existingShares.find(
                            (s, i) =>
                              s.inputString === share.inputString && i !== index
                          )
                        ) {
                          share.isValid = false
                        }
                      }
                      this.setState({ shares: existingShares })
                    },
                    errorMessage:
                      share.inputString.length && !share.isValid
                        ? text.decryptedSharesInput.invalid.replace(
                            '%d',
                            (MIN_SHARE_LENGTH * 2).toString()
                          )
                        : undefined,
                  }),
                ])
              }),
              h('div', { className: 'my-1' }, [
                h('span', { className: 'mr-2'},
                  h(Button, {
                    text: text.addButtonText,
                    textOutsideBox: true,
                    onClick: () => {
                      const existingShares = this.state.shares
                      if (existingShares.length < MAX_SHARES) {
                        existingShares.push(new ShareInput())
                      }
                      this.setState({ shares: existingShares })
                    },
                    icon: icons('plus'),
                  })
                ),
                h(Button, {
                  text: text.removeButtonText,
                  textOutsideBox: true,
                  disabled: this.state.shares.length < 3,
                  onClick: () => {
                    const existingShares = this.state.shares
                    existingShares.pop()
                    this.setState({ shares: existingShares })
                  },
                  icon: icons('minus'),
                }),
              ])
            ]),
            h('div', { className: 'flex justify-between mt-4' }, [
              h('div'),
              h('div', [
                h('span', { className: 'mr-4' }, [
                  h(Button, {
                    onClick: this.recover,
                    disabled: !this.canRecover(),
                    text: text.recoverSecretButton,
                    icon: icons('cofre-abierto'),
                  }),
                ]),
                this.props.testMode
                  ? h(Button, {
                      onClick: this.addTestData,
                      text: 'Add test data',
                      icon: icons('plus'),
                    })
                  : undefined
              ]),
            ]),
            this.state.recoveryErrMessage.length
              ? h(ErrorMessage, this.state.recoveryErrMessage)
              : undefined,
          ]),
    ])
  }

  // Are we able to recover yet?
  canRecover() {
    return this.state.shares.filter((s) => s.isValid).length > 1
  }

  // tryRecover() {
  //   try {
  //     dc.recover(
  //       this.state.shares.filter((s) => s.isValid).map((s) => s.inputString)
  //     )
  //   } catch (err) {
  //     console.log('Error from recover fn', err)
  //     // TODO check type of error
  //     return false
  //   }
  //   return true
  // }

  recover() {
    try {
      const secret = this.props.dc.recover(
        this.state.shares.filter((s) => s.isValid).map((s) => s.inputString)
      )
      this.setState({
        secret,
        shares: [new ShareInput(), new ShareInput()],
        recoveryErrMessage: '',
      })
    } catch (error) {
      const err = error as any // Should this be `Error`?
      console.log('Error from recover fn', err)
      this.setState({
        recoveryErrMessage: err.message.startsWith('aead')
          ? text.recoveryDecryptionError
          : err.message,
        secret: '',
      })
    }
  }

  addTestData() {
    this.setState({
      shares: [
        new ShareInput(
          '02beb726fa3acfd0b148fd1b77da2e1daa6700272868049c6e14affb728da15ffd0c5f8359d724ed303748556778b1bf0171b9317773a614e21726aa25415d5f3a071f9c6f3b4c1d5eec62339ab7f8755212792d7fa36d4bf0b2'
        ),
        new ShareInput(
          '0476f084048efb24226a330c7aef84007eac83a88ee129c193ea765a282f7c57780c5f8359d724ed303748556778b1bf0171b9317773a614e21726aa25415d5f3a071f9c6f3b4c1d5eec62339ab7f8755212792d7fa36d4bf0b2'
        ),
        new ShareInput(
          '059c5a5dbfe39f7dc08547a713ca047aebf2b637291c1ad6d28cbdae4d452a3ad30c5f8359d724ed303748556778b1bf0171b9317773a614e21726aa25415d5f3a071f9c6f3b4c1d5eec62339ab7f8755212792d7fa36d4bf0b2'
        ),
      ],
    })
  }
}

class ShareInput {
  inputString: string
  isValid: boolean

  constructor(inputString?: string) {
    this.inputString = '' // Just to make typescript not bark
    this.isValid = true
    this.updateInputString(inputString || '')
  }

  updateInputString(newInputString: string) {
    this.inputString = newInputString.trim()
    this.isValid =
      Buffer.from(this.inputString, 'hex').length > MIN_SHARE_LENGTH
  }
}
