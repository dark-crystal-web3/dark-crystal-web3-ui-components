import React from 'react'
import h from '../services/hyperscript'

// Numbered heading for form section. The number is passed as a property.

type Props = {
  number?: number,
  text: string | React.ReactNode,
}

const NumberedHeading = ({ number, text }: Props) => {
  return h('div', { className: 'my-6 flex items-center' }, [
    number
      ? h(
          'strong',
          {
            className:
              'font-silkscreen text-3xl bg-very-dark-pink text-very-pale-pink py-1 px-box rounded',
          },
          number.toString()
        )
      : undefined,
    h(
      'h2',
      {
        className: 'font-silkscreen text-2xl text-very-dark-pink w-full m-1 px-1 uppercase',
      },
      text
    ),
  ])
}

export default NumberedHeading
