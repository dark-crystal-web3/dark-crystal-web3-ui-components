import React from 'react'
import icons from '../shared/icons'
import h from '../services/hyperscript'

type Props = {
  children: React.ReactNode,
}

const ErrorMessage = ({ children }: Props) => {
  return h('div', { className: 'flex items-center' }, [
      h('span', { className: 'bg-pink-600 m-1 p-1 rounded' }, icons('skull')),
      h(
        'h2',
        {
          className:
            'font-silkscreen text-pink-600 w-full m-1 font-bold uppercase',
        },
        children
      ),
    ])
}

export default ErrorMessage
