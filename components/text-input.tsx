import React from 'react'
import h from '../services/hyperscript'

// Text input box

type Props = {
  icon?: React.ReactNode,
  value: string,
  placeholder?: string,
  onChange: Function,
  errorMessage?: string,
}

type State = {
  focussed: boolean,
}

export default class TextInput extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      focussed: false
    }
  }

  render() {
    const textBgColor = this.state.focussed ? 'pink-600' : 'very-dark-pink'
    const outline = this.state.focussed ? 'outline outline-pink-500' : ''
    return h('div', { className: 'py-1' }, [
      h('div', { className: `flex md:items-center border-2 border-very-dark-pink rounded ${outline}` }, [
        this.props.icon
          ? h(
              'div',
              { className: 'bg-very-dark-pink p-1 md:mr-1' },
              this.props.icon
            )
          : undefined,
        h(
          'input',
          {
            className:
              `w-full mx-2 px-1 mr-2 text-sm bg-inherit my-1 placeholder-very-dark-pink focus:outline-none focus:placeholder-pink-600 text-${textBgColor} font-mono`,
            type: 'text',
            placeholder: this.props.placeholder,
            value: this.props.value,
            onChange: this.props.onChange,
            onFocus: () => {
              this.setState({ focussed: true })
            },
            onBlur: () => {
              this.setState({ focussed: false })
            },
          },
        ),
      ]),
      this.props.errorMessage
        ? h('div', [h('strong.text-pink-600', this.props.errorMessage)])
        : undefined,
    ])
  }
}
