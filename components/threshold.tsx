import React from 'react'
import h from '../services/hyperscript'
import icons from '../shared/icons'
import text from '../services/text'
import TextBubble from './text-bubble'

// Visualiser and text for threshold

// Icon colours
const colours: Array<string> = shuffle(['Beige', 'Blue', 'Brown', 'Green', 'Pink', 'Turquoise'])

type Props = {
  m: number,
  n: number,
  onChange: Function,
}

type State = {
  threshold: number,
}

export default class Threshold extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      threshold: props.m || defaultThreshold(props.n),
    }
  }

  componentDidUpdate(prevProps: Props) {
    if (prevProps.n !== this.props.n) {
      const threshold = defaultThreshold(this.props.n)
      this.setState({ threshold })
      if (typeof this.props.onChange === 'function') {
        this.props.onChange(threshold)
      }
    }
  }

  render() {
    if (this.props.n < 2) return null


    const thresholdVisualiser = new Array(this.props.n)
      .fill(0)
      .map((_e, index) => {
        const opacity = index >= this.state.threshold ? 50 : 100
        return h('div', { className: `bg-very-dark-pink p-1 m-1 rounded opacity-${opacity}`}, [
          icons(`crystal${colours[index % colours.length]}`)
        ])
      })

    return h(TextBubble, {
      additionalContent: h('div', [
        h('div', { className: 'flex justify-start' }, thresholdVisualiser),
          this.props.n > 4 && !this.props.m
            ? h('div', [
                h('input', {
                  type: 'range',
                  className: 'accent-very-dark-pink focus:accent-strong-pink',
                  min: 2,
                  max: this.props.n - 1,
                  value: this.state.threshold,
                  onChange: (event: React.ChangeEvent<HTMLInputElement>) => {
                    this.setState({ threshold: parseInt(event.target.value) })
                    this.props.onChange(parseInt(event.target.value))
                  },
                }),
              ])
            : undefined,
      ]),
      bullets: text.thresholdText.map((t) => {
        return t.replace('%d', this.state.threshold.toString()).replace('%d', this.props.n.toString())
      })
    })
  }
}

function defaultThreshold(n: number) {
  if (n === 2) return 2
  return Math.floor(n * 0.75)
}

function shuffle(array: Array<any>) {
  for (let i = array.length - 1; i > 0; i--) {
    let j = Math.floor(Math.random() * (i + 1))
    let t = array[i]
    array[i] = array[j]
    array[j] = t
  }
  return array
}
