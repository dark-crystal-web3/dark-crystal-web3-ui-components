import text from '../services/text'
import icons from '../shared/icons'
import h from '../services/hyperscript'

// App title with logo

const AppTitle = () => {
  return h('div', { className: 'flex' }, [
    h(
      'div',
      {
        className:
        'hidden md:block bg-very-dark-pink shadow-[-8px_8px_0px_rgba(0,0,0,0.3)] shadow-pink-600 p-2 m-1',
      },
      [icons('crystals-plenty')]
    ),
    h('div', { className: 'mb-4' }, [
      h(
        'h1',
        {
          className:
          'font-silkscreen uppercase text-3xl font-semibold',
        },
        text.appName
      ),
      h('p.text-sm', text.appSubtext),
    ]),
  ])
}

export default AppTitle
