import React from 'react'
import h from '../services/hyperscript'

// Button

type Props = {
  disabled?: boolean,
  active?: boolean,
  onClick: Function,
  title?: string,
  icon?: React.ReactNode,
  text?: string,
  textOutsideBox?: boolean,
  ariaLabel?: string,
}

const Button = ({ onClick, disabled, active, title, icon, text, textOutsideBox, ariaLabel}: Props) => {
  const opacity = disabled ? 'opacity-50' : 'opacity-100'
  const hoverBg = active || disabled ? '' : 'group-hover:bg-pink-600'
  const hoverUnderline = disabled ? '' : 'hover:underline'
  const hoverText = disabled ? '' : 'hover:text-pink-600'

  const textSection = active
            ? h('strong', { className: 'text-lg p-2 text-pink-600' }, text)
            : h('span', { className: 'text-lg p-2' }, text)

  return h(
    'button',
    { onClick,
      disabled,
      title,
      'aria-label': ariaLabel || text
    },
    [
      h('div', {
        className: `group flex ${opacity} text-very-dark-pink ${hoverText} font-silkscreen uppercase ${hoverUnderline}`
      }, [
        h('div', {
            className: `flex rounded text-white bg-very-dark-pink ${hoverBg} ${hoverUnderline}`,
          }, [
          icon
            ? h('span', { className: 'p-1', }, icon)
            : undefined,
          text && !textOutsideBox
            ? textSection
            : undefined
        ]),
        text && textOutsideBox ? textSection : undefined
      ]),
    ]
  )
}

export default Button
