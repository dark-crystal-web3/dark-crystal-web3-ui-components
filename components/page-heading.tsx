import React from 'react'
import h from '../services/hyperscript'

// Top of page heading with optional icon

type Props = {
  text: string,
  subtext: string | React.ReactNode,
  subsubtext?: string | React.ReactNode,
  icon?: React.ReactNode,
}

const PageHeading = ({ text, subtext, subsubtext, icon }: Props) => {
  return h('div', { className:  'container mx-auto' }, [
    h('div', { className: 'flex items-center pb-4' }, [
      icon
        ? h('span', { className: 'bg-very-dark-pink m-1 p-1 rounded' }, icon)
        : undefined,
      h(
        'h2',
        {
          className:
            'font-silkscreen w-full m-1 text-3xl font-bold uppercase',
        },
        text
      ),
    ]),
    h('strong', { className: 'text-base' }, subtext),
    h('p', { className: 'text-sm' }, subsubtext),
  ])
}

export default PageHeading
